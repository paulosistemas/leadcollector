package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import net.bytebuddy.dynamic.loading.InjectionClassLoader;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nome;
    private String email;
    private LocalDate dataCadastro;
    private TipoLeadEnum tipoLead;

    public Lead() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }
}
